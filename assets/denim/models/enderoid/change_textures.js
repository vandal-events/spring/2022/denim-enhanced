// This tool is to convert every single Enderoid model to be moved to the denim namespace.

const fs = require('fs');

const textureName = 'denim:enderoid/enderoid';

fs.readdirSync('./').forEach(modelName => {
    if (!modelName.endsWith('.json')) return;

    let model = JSON.parse(fs.readFileSync(`./${modelName}`));

    model.textures['0'] = textureName;
    model.textures.particle = textureName;
    
    fs.writeFileSync(`./${modelName}`, JSON.stringify(model, null, 4));

    console.log(`Wrote ${modelName}`);
});