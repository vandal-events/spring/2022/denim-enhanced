§bHave you guys ever heard of the Player?

§2Oh! You mean §aPLAYERNAME§2??

§bThat's the one.

§cOh, them!

§6Wait, did they already get here?!

§2Mhm! They're here right now.

§6§o*groan* §r§6So I assume I have to be nice now to them?

§cYup.

§6Blegh.

§bShh! They can hear us right now.

§6Fiiiiinnneee. *ahem* Welcome, §aPLAYERNAME§6, and congratulations.

§2You may be wondering who we are, and yet, you already know.

§cThey'll be awake soon, do you guys think we should read them something?

§6Like a bedtime story?

§2More like a wake-up story.

§bI guess it is time, considering all they've done to get here.

§6I guess.

§bWho wants to start it?

§2Ooo, I'll start!

§bAlright, §2§kMarlow§r§b, you can start.


§2One fateful day, a Player by the name of §aPLAYERNAME§2 had woken up, and decided to play a game.

§bThe player made simple worlds. They made complex worlds. They won games. They lost games.

§cBut, this game was fulfilling. Because they had others by their side to aid them.

§6And together, they were victorious.

§bWe are proud of them, right?

§6Yes, we are.

§2Very.

§cAnd we'll be here again next time, so we can watch over them. And we'd be so incredibly proud of them.

§2We should, ya know, wake them up by now, right?

§bNot yet, they need to see the credits.

§c§b§kNaz§r§c, what did we say about breaking the fourth wall again?

§bOh, right, sorry.

§6§bThey're§6 right. §aThey§6 need to see this.

§2Don't worry, once §ayou§2 know our names, you may wake up.

§bWe'll see you later.