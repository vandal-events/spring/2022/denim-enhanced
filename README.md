# DenimEnhanced

This is the official repository for Denim's resource pack, and may be used for personal purposes for projects, provided you provide credit to us, using [this format](#credit).

The scripts provided in this repository have no real purpose for Minecraft outside of developer usage, but are retained for reference purposes. If you have no idea how to use these scripts, they are fundamentally useless for you.

## Attribution Example
You may copy the following for providing attribution for using any asset in this repository.

```
This project uses assets from DenimEnhanced, provided by Vandal Events [https://vandal.events]
The assets may be found at the repository at https://gitlab.com/vandal-events/spring-2022/denim-enhanced.
```

## Credits
Music: [Solunary](https://www.youtube.com/c/solunary) - [The End at Hand](https://www.youtube.com/watch?v=GAW5tuC83mE)
